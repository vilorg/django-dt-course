# Template for backend course

### Installation:
###### Run `pipenv install` _(you can install to a local venv if it exists)_


### Launching:
###### Run `make dev` to launch django app in dev mode
###### Run `make bot` to launch telegram bot
#

#### `admin/` - admin route
#### `api/me?user_id=123` - get person info route
#

### Остались вопросы? Тг - `@vilorg`
