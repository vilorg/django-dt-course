from app.internal.models.person import Person


def save_user(person: Person) -> bool:
    if not Person.objects.filter(user_id=person.user_id).exists():
        person.save()
        return True
    return False


def update_profile(user_id: int, phone: str):
    if not Person.objects.filter(user_id=user_id).exists():
        raise Exception("Profile does not exists")
    person = Person.objects.get(user_id=user_id)
    person.phone = phone
    person.save()


def get_profile(user_id: int) -> Person:
    if not Person.objects.filter(user_id=user_id).exists():
        raise Exception("Profile does not exists")
    person = Person.objects.get(user_id=user_id)
    if person.phone is None:
        raise Exception("Profile is not full")
    return Person.objects.get(user_id=user_id)


def is_full_user(user_id: int) -> bool:
    person = Person.objects.get(user_id=user_id)
    if person.phone is not None:
        return True
    return False
