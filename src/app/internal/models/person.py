from django.db import models
from django.http import JsonResponse


class Person(models.Model):
    first_name = models.CharField(max_length=255, verbose_name="Имя", null=True)
    last_name = models.CharField(max_length=255, verbose_name="Фамилия", null=True)
    username = models.CharField(max_length=255, verbose_name="Username", null=True)
    phone = models.CharField(max_length=255, verbose_name="Номер телефона", blank=True, null=True)
    user_id = models.IntegerField(verbose_name="ID пользователя в телеграмме", blank=False, null=False, unique=True)

    def is_full(self):
        return self.phone is not None

    def get_person_dict(self) -> dict:
        return {'user_id': self.user_id, 'first_name': self.first_name, 'last_name': self.last_name,
                'username': self.username, 'phone': self.phone}

    class Meta:
        verbose_name = "Профиль"
        verbose_name_plural = "Профили"

    def __str__(self):
        return f'First name: {self.first_name}, Second name: {self.last_name} - username: {self.username}, ' \
               f'phone number: {self.phone}'

    def __init__(self, first_name: str, last_name: str, username: str, user_id: int, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.first_name = first_name
        self.last_name = last_name
        self.username = username
        self.user_id = user_id

    # noinspection PyRedeclaration
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
