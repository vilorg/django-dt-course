from django.contrib import admin
from app.internal.models.person import Person


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    fields = ('username', 'first_name', 'last_name', 'phone', 'user_id', 'is_full',)
    readonly_fields = ('is_full', 'user_id', 'username')

    @staticmethod
    def is_full(obj: Person):
        return obj.is_full()
