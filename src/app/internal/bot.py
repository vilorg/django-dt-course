#!/usr/bin/env python
import logging

import telebot
import os
from pathlib import Path

import environ
from telebot.types import *

from app.internal.transport.bot.handlers import *

# importing path
base_dir = Path(__file__).resolve().parent.parent.parent
env = environ.Env()
env.read_env(os.path.join(base_dir.parent, '.env'))

token = env('TOKEN_BOT')

# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__name__)

bot = telebot.TeleBot(token)


# Handle '/start'
@bot.message_handler(commands=['start'])
def send_welcome(message: Message):
    try:
        user = message.from_user
        is_success = start(first_name=user.first_name, last_name=user.last_name, username=user.username,
                           user_id=user.id)
        if is_success:
            bot.reply_to(message, "Success")
        else:
            bot.reply_to(message, "Your user already exits")
    except Exception as e:
        print(e)
        bot.reply_to(message, e.__str__())


@bot.message_handler(commands=['set_phone'])
def set_phone(message: Message):
    keyboard = ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
    button_phone = KeyboardButton(text="Send phone", request_contact=True)
    keyboard.add(button_phone)
    bot.send_message(message.chat.id, 'Phone number', reply_markup=keyboard)


@bot.message_handler(content_types=['contact'])
def contact(message: Message):
    if message.contact is not None:
        try:
            update_profile(user_id=message.from_user.id, phone=message.contact.phone_number)
            bot.reply_to(message, "Your phone number successfully added to your profile!")
        except Exception as e:
            print(e)
            bot.reply_to(message, e.__str__())
    else:
        bot.reply_to(message, "Your contact empty. :(")


@bot.message_handler(commands=['me'])
def me(message: Message):
    try:
        profile = get_profile(message.from_user.id)
        bot.reply_to(message, profile.__str__())
    except Exception as e:
        bot.reply_to(message, e.__str__())


def main():
    bot.infinity_polling()
