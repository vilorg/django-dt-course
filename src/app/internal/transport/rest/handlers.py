from django.core.handlers.wsgi import WSGIRequest
from django.core.serializers.json import DjangoJSONEncoder
from django.http import JsonResponse
from django.utils.datastructures import MultiValueDictKeyError
from django.views.generic import ListView

from app.internal.models.person import Person


class GetTelegramInfoView(ListView):
    model = Person

    def get(self, request: WSGIRequest, *args, **kwargs):
        user_id = -1
        is_integer_user = True
        try:
            user_id = int(request.GET['user_id'])
        except MultiValueDictKeyError:
            user_id = -1
        except Exception as e:
            user_id = -2
            is_integer_user = False
            print(e)

        if user_id == -1:
            return JsonResponse(
                {'response': {'success': False, 'data': {}, 'message': 'user_id required field'}})
        if not is_integer_user:
            return JsonResponse(
                {'response': {'success': False, 'data': {}, 'message': 'user_id is not integer'}})

        try:
            user = Person.objects.get(user_id=user_id)
            return JsonResponse({'response': {'success': True, 'data': user.get_person_dict(), 'message': ''}})
        except Exception as e:
            print(e)
            return JsonResponse({'response': {'success': False, 'data': {}, 'message': 'user does not exists'}})
