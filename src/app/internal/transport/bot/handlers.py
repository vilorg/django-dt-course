from app.internal.models.person import Person
from app.internal.services import person_service


def start(first_name: str, last_name: str, username: str, user_id: int) -> bool:
    person = Person(first_name=first_name, last_name=last_name, username=username, user_id=user_id)
    return person_service.save_user(person)


def update_profile(user_id: int, phone: str):
    return person_service.update_profile(user_id, phone)


def get_profile(user_id: int):
    return person_service.get_profile(user_id)
