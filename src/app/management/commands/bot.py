from django.core.management import BaseCommand

from app.internal.bot import main


class Command(BaseCommand):
    help = "Telegram-bot"

    def handle(self, *args, **options):
        main()
